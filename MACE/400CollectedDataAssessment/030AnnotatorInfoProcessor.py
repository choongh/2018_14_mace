# Author: Han Xiang Choong
# Date: 08/04/2018
# File: 030AnnotatorInfoProcessor.py
# Status: Ongoing
# Description: Average out an annotator's trustworthiness and accuracy ratings across all questions, then add
#              this to the trial report. 


import csv
import sys

inputPath = sys.argv[1]
outputPath = sys.argv[2]

# First, initialize a dictionary with all of the annotator IDs mapped to basic, empty variables, which will 
# subsequently be filled. 
def createAnnotatorDictionary(inputPath):
    outputDict = {}
    with open(inputPath, "rU") as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if row[2] == " bad":
                outputDict.update({row[6].replace("'", "").strip() : {"trustworthiness" : [], "trueLabels" : [], "answers" : [], "accuracy" : 0.0, "averageTrust" : 0.0}})
            else:
                outputDict.update({row[4].replace("'", "").strip() : {"trustworthiness" : [], "trueLabels" : [], "answers" : [], "accuracy" : 0.0, "averageTrust" : 0.0}})

    return outputDict

# For each annotator, append all of the relevant information to the dictionary created above. Each annotator is
# associated with an array of all of the trustworthiness variables and labels which that annotator gave, as well
# as the trueLabels of the questions which the annotator answered. 
def assessAnnotators(inputDict):
    outputDict = inputDict
    with open(inputPath, "rU") as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if row[2] == " bad":
                annotator = row[6].replace("'", "").strip()
                trueLabels = outputDict[annotator]["trueLabels"]
                trueLabels.append(row[4].strip())
                answers = outputDict[annotator]["answers"]
                answers.append(row[5].strip())
                trustWorthiness = outputDict[annotator]["trustworthiness"]
                trustWorthiness.append(row[7].strip())
                outputDict.update({annotator : {"trustworthiness" : trustWorthiness, "trueLabels" : trueLabels, "answers" : answers, "accuracy" : 0.0, "averageTrust" : 0.0}})
            else:
                annotator = row[4].replace("'", "").strip()
                trueLabels = outputDict[annotator]["trueLabels"]
                trueLabels.append(row[2].strip())
                answers = outputDict[annotator]["answers"]
                answers.append(row[3].strip())
                trustWorthiness = outputDict[annotator]["trustworthiness"]
                trustWorthiness.append(row[5].strip())
                outputDict.update({annotator : {"trustworthiness" : trustWorthiness, "trueLabels" : trueLabels, "answers" : answers, "accuracy" : 0.0, "averageTrust" : 0.0}})
    return outputDict

# This function iterates through each annotator's trust parameters, sums them, and then averages them out. 
def averageTrustWorthiness(inputDict):
    outputDict = inputDict
    for annotator in outputDict:
        trustSum = 0
        for trustParam in outputDict[annotator]["trustworthiness"]:
            trustSum += float(trustParam)
        outputDict[annotator].update({"averageTrust" : trustSum / len(outputDict[annotator]["trustworthiness"])})
    return outputDict

# This function does the same as the above, but for accuracy. It compares trueLabels with the annotator's
# given answers, and uses the number of matches as an accuracy rating. 
def averageAccuracy(inputDict):
    outputDict = inputDict
    for annotator in outputDict:
        accuracySum = 0
        for index in range(0, len(outputDict[annotator]["trueLabels"])):
            if outputDict[annotator]["trueLabels"][index] == outputDict[annotator]["answers"][index]:
                accuracySum += 1.0
        outputDict[annotator].update({"accuracy" : accuracySum / len(outputDict[annotator]["trueLabels"])})
    return outputDict

# Write the results out to a csv file. 
def writeMetaResultsToCSV(inputDict, outputPath):
    with open(outputPath, "a") as outputFile:
        writer = csv.writer(outputFile)
        writer.writerow(["------------------"])
        writer.writerow(["Annotator Report--"])
        writer.writerow(["Number of Annotators: " + str(len(inputDict))])
        writer.writerow(["Annotator", "Trust", "Accuracy"])
        for annotator in inputDict:
            writer.writerow([annotator, inputDict[annotator]["averageTrust"], inputDict[annotator]["accuracy"]])


writeMetaResultsToCSV(averageAccuracy(averageTrustWorthiness(assessAnnotators(createAnnotatorDictionary(inputPath)))), outputPath)