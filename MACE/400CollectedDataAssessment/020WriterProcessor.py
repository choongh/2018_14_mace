# Author: Han Xiang Choong
# Date: 08/03/2018
# File: 020WriterProcessor.py
# Status: Ongoing
# Description: Take in accuracy ratings and meta information about the data, and write it out to 
#              a consolidated report. 

import csv
import sys
import time

sourceFile = sys.argv[1]
trialLabel = sys.argv[2]
acc = sys.argv[3]
outputPath = sys.argv[4]
inputForQuestionNumber = sys.argv[5]
timingPath = sys.argv[6]

def writeReport(sourceFile, trialLabel, acc, outputPath, inputForQuestionNumber, timingPath):

    # Count up the number of questions in order to place this info in the report. 
    questionCounter = 0
    with open(inputForQuestionNumber, "rU") as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if row[0] != 'Question':
                questionCounter += 1
    inputFile.close()
    # Grab timings from the time file. 
    timeArray = []
    with open(timingPath, "rU") as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if "TimeStamp" in row[0]:
                timeArray.append(row[0].replace("TimeStamp: ", ""))
    inputFile.close()

    with open(outputPath, "a") as outputFile:
        writer = csv.writer(outputFile)
        writer.writerow(["------------------"])
        writer.writerow(["Main Report-------"])
        writer.writerow(["Training Began on: " + timeArray[0]])
        writer.writerow(["Training Ended on: " + timeArray[1]])
        writer.writerow(["Created on: " + time.asctime( time.localtime(time.time()) )])
        writer.writerow(["Source File: " + sourceFile])
        writer.writerow(["Trial: " + trialLabel])
        writer.writerow(["Accuracy: " + acc])
        writer.writerow(["Number of Questions: " + str(questionCounter)])

writeReport(sourceFile, trialLabel, acc, outputPath, inputForQuestionNumber, timingPath)