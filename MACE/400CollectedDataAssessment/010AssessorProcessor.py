# Author: Han Xiang Choong
# Date: 07/23/2018
# File: 010AssessorProcessor.py
# Status: Ongoing
# Description: Assess accuracy of MACE by comparing predicted labels to actual labels

import csv
import time
import sys

baseComparator = sys.argv[1]
predictions = sys.argv[2]
outputPath = sys.argv[3]

# This function takes in the count file, which contains the distribution of answers given to questions by users. 
# It turns this into a dictionary, with the questions as the keys, and the index of the most popular answer for that question
# as values.
def prepareAnswerKeyFromCounts(countFile):
	dictionaryOfAnswers = {}
	with open(countFile, 'rU') as base:
		reader = csv.reader(base)
		# For each row (question) in the count file, we'll need to take the most popular prediction. 
		for response in reader:
			# Ignore headers
			if response[0] != "Question":
				# Arrange the distribution of answers in a list format, and convert from string to integer.
				arrayOfResponses = [int(response[1]), int(response[2]), int(response[3]), int(response[4]), int(response[5]), int(response[6]), int(response[7]), int(response[8])]
				# Now find the maximum value and take its index. This index represents the answer. 
				dictionaryOfAnswers.update({response[0].strip() : str(arrayOfResponses.index(max(arrayOfResponses))) })	
	return dictionaryOfAnswers

# This function assesses accuracy by taking the two dictionaries created above, counts the number of times they agree on the answer to 
# a question, and returns this as a percentage measure of accuracy.
def assessAccuracy(baseComparator, predictions, outputPath):
	counter = 0
	answerKey = prepareAnswerKeyFromCounts(baseComparator)
	predictionSet = prepareAnswerKeyFromCounts(predictions)
	wrong = []
	
	for key in predictionSet:
		if answerKey[key] == predictionSet[key]:
			counter += 1
		else:
			wrong.append([key, answerKey[key], predictionSet[key]])

	with open(outputPath, "w") as outputFile:
		writer = csv.writer(outputFile)
		writer.writerow(["------------------"])
		writer.writerow(["Wrong Answers-----"])
		writer.writerow(["Question", "CorrectAnswer", "AnswerGiven"])
		for wrongEntry in wrong:
			writer.writerow(wrongEntry)

	return str(counter/ len(answerKey.keys()))

# Send the accuracy rating to bash. 
sys.exit(assessAccuracy(baseComparator, predictions, outputPath))