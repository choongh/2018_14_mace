# Author: Han Xiang Choong
# Date: 07/30/2018
# File: 011QuestionSetExpander.py
# Status: Ongoing
# Description: The purpose of this file is to expand the prediction returned by the model working with 
#              the QuestionSets format, by extracting the array-strings associated with each question
#              into individual annotations in the same form as the other dataSet formats. 


import csv
import sys
inputPath = sys.argv[1]
outputPath = sys.argv[2]

def expandToStandardizedFormat(inputPath, outputPath):
    with open(inputPath, "rU") as inputFile:
        with open(outputPath, "w") as outputFile:
            reader = csv.reader(inputFile)
            writer = csv.writer(outputFile)
            counter = 0
            for row in reader:
                counter += 1
                index = 5
                prompt = row[1] + "," + row[2] + "," + row[3]
                trueLabel = row[4]
                for i in range(index, len(row[index:]) - 2):
                    if ((i + 1) % 3) == 0:
                        writer.writerow([str(0), prompt, trueLabel, row[i + 1], row[i], row[i + 2]])
                index += 3

expandToStandardizedFormat(inputPath, outputPath)