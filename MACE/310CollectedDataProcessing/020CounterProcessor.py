# Author: Han Xiang Choong
# Date: 08/03/2018
# File: 020CounterProcessor.py
# Status: Ongoing
# Description: The purpose of this file is to create a count file for the predictions returned by the model.
#              This countfile will prepare a distribution of predicted trueLabels for a question, which will
#              be compared to the most popular responses in the original dataSet, and scored for number of matches. 

import csv
import sys
inputPath = sys.argv[1]
outputPath = sys.argv[2]
nChoices = int(sys.argv[3])

# This helper produces a dictionary which holds a vector of answers to a single question.
def initializeCounterDictionary(nChoices):
    output = {}
    for i in range(0, nChoices):
        output.update({str(i) : 0})
    return output

# This helper populates a dictionary with the quiz questions. 
def initializeQuestionDictionary(inputPath):
    output = {}
    with open(inputPath, "rU") as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if row[2] == " bad":
                prompt = row[1] + "," + row[2] + "," + row[3]
            else:
                prompt = row[1]
            output.update({prompt : initializeCounterDictionary(nChoices)})
    return output

# This function uses the above dictionaries to count up the answers given to a question across an entire set of predictions. 
def preparePredictionCounts(inputPath, outputPath, nChoices):
    # First, open the output file and prepare to write to it.
    with open(outputPath, "w", newline='') as outputFile:
        writer = csv.writer(outputFile)
        # Write down the header.
        writer.writerow(["Question","Good","Bad","Neutral","Don't Know Meaning", "Depends","Ungrammatical","Other", "Do not know verb"])
        # Initialize dictionary for holding questions.
        questionDictionary = initializeQuestionDictionary(inputPath)
        # For each question in the quiz, we need to read through all of the rows in the set of predictions. 

        # Open the inputfile, which stands for the set of predictions. 
        with open(inputPath, "rU") as inputFile:
            reader = csv.reader(inputFile)
            # Initialize the dictionary to hold a running count of answers.
            for row in reader:
                # Dealing with a condition in which the question is segmented into three parts instead of being kept
                # as a single string. 
                if row[2] == " bad":
                    prompt = row[1] + "," + row[2] + "," + row[3]
                    trueLabel = row[5]
                    currentCount = questionDictionary[prompt][trueLabel] + 1
                    questionDictionary[prompt].update({trueLabel : currentCount})
                # This else activates if the question is kept as one string. 
                else:
                    prompt = row[1]
                    trueLabel = row[3]
                    currentCount = questionDictionary[prompt][trueLabel] + 1
                    questionDictionary[prompt].update({trueLabel : currentCount})
        # After all annotations are accounted for, write out to dictionary. 
        for question in questionDictionary:
            writer.writerow([question, questionDictionary[question]["0"], questionDictionary[question]["1"], questionDictionary[question]["2"], questionDictionary[question]["3"], questionDictionary[question]["4"], questionDictionary[question]["5"], questionDictionary[question]["6"], questionDictionary[question]["7"]])

preparePredictionCounts(inputPath, outputPath, nChoices)