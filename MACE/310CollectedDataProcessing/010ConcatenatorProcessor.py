# Author: Han Xiang Choong
# Date: 07/30/2018
# File: 010ConcatenatorProcessor.py
# Status: Ongoing
# Description: Take multiple CSVs representing the results of ten threads, and concatenate those results
#               by appending to a single CSV file. 
#               (dataPostTraining/AGWSegment/results/experimentLabel/concatenatedData/concatenated.csv)

import csv
import glob
import sys

inputPath = sys.argv[1]
outputPath = sys.argv[2]
# Use glob to iterate through all of the collected data from each thread, and write the contents
# of each csv file to a common output, 100Concatenated.csv
def concatenate(inputPath, outputPath):
    with open(outputPath, "w", newline='') as outputFile:
        for resultFolder in glob.glob(inputPath + '/*'):
            for resultFile in glob.glob(resultFolder + '/*'):
                writer = csv.writer(outputFile)
                with open(resultFile, "rU") as inputFile:
                    reader = csv.reader(inputFile)
                    for row in reader:
                        writer.writerow(row)

concatenate(inputPath, outputPath)