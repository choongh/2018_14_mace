import time
import csv
import sys

# This is a simple helper function for writing a timeStamp to a csv with an event
# label denoting what the timeStamp represents. 
def timeStamp(outputPath, label, trialName):
    with open(outputPath, 'a', newline='') as outputBase:
        writer = csv.writer(outputBase)
        writer.writerow(["------------------"])
        writer.writerow(["Status Label: " + label])
        writer.writerow(["TimeStamp: " + time.asctime( time.localtime(time.time()) )])
        writer.writerow(["Trial Label: " + trialName])

timeStamp(sys.argv[1], sys.argv[2] + " " + sys.argv[3], sys.argv[4])