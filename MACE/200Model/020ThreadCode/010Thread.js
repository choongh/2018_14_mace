var webppl = require("/usr/local/lib/node_modules/webppl/src/main.js");
var args = require("/usr/local/lib/node_modules/webppl/src/args.js");
args.makeGlobal(__filename, process.argv.slice(2));
var webpplCsv = require("/Users/hanxiangchoong/.webppl/node_modules/webppl-csv");
var __runner__ = util.trampolineRunners.cli();
function topK(s, x) {
  console.log(x);
};
var main = (function (_globalCurrentAddress) {
    return function (p) {
        return function (runTrampoline) {
            return function (s, k, a) {
                runTrampoline(function () {
                    return p(s, k, a);
                });
            };
        };
    }(function (globalStore, _k0, _address0) {
        var _currentAddress = _address0;
        _addr.save(_globalCurrentAddress, _address0);
        var Bernoulli = dists.makeBernoulli;
        var Beta = dists.makeBeta;
        var beta = function beta(globalStore, _k424, _address2, arg0, arg1) {
            var _currentAddress = _address2;
            _addr.save(_globalCurrentAddress, _address2);
            var _k426 = function (globalStore, params) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                return function () {
                    return Beta(globalStore, function (globalStore, _result425) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        return function () {
                            return sample(globalStore, _k424, _address2.concat('_3'), _result425);
                        };
                    }, _address2.concat('_2'), params);
                };
            };
            return function () {
                return util.isObject(arg0) ? _k426(globalStore, arg0) : _k426(globalStore, {
                    a: arg0,
                    b: arg1
                });
            };
        };
        var Binomial = dists.makeBinomial;
        var Categorical = dists.makeCategorical;
        var categorical = function categorical(globalStore, _k418, _address4, arg0, arg1) {
            var _currentAddress = _address4;
            _addr.save(_globalCurrentAddress, _address4);
            var _k420 = function (globalStore, params) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                return function () {
                    return Categorical(globalStore, function (globalStore, _result419) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        return function () {
                            return sample(globalStore, _k418, _address4.concat('_7'), _result419);
                        };
                    }, _address4.concat('_6'), params);
                };
            };
            return function () {
                return util.isObject(arg0) ? _k420(globalStore, arg0) : _k420(globalStore, {
                    ps: arg0,
                    vs: arg1
                });
            };
        };
        var Cauchy = dists.makeCauchy;
        var Delta = dists.makeDelta;
        var DiagCovGaussian = dists.makeDiagCovGaussian;
        var Dirichlet = dists.makeDirichlet;
        var dirichlet = function dirichlet(globalStore, _k406, _address8, arg0) {
            var _currentAddress = _address8;
            _addr.save(_globalCurrentAddress, _address8);
            var _k408 = function (globalStore, params) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                return function () {
                    return Dirichlet(globalStore, function (globalStore, _result407) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        return function () {
                            return sample(globalStore, _k406, _address8.concat('_15'), _result407);
                        };
                    }, _address8.concat('_14'), params);
                };
            };
            return function () {
                return util.isObject(arg0) ? _k408(globalStore, arg0) : _k408(globalStore, { alpha: arg0 });
            };
        };
        var Discrete = dists.makeDiscrete;
        var Exponential = dists.makeExponential;
        var Gamma = dists.makeGamma;
        var Gaussian = dists.makeGaussian;
        var ImproperUniform = dists.makeImproperUniform;
        var IspNormal = dists.makeIspNormal;
        var KDE = dists.makeKDE;
        var Laplace = dists.makeLaplace;
        var LogisticNormal = dists.makeLogisticNormal;
        var LogitNormal = dists.makeLogitNormal;
        var Marginal = dists.makeMarginal;
        var Mixture = dists.makeMixture;
        var Multinomial = dists.makeMultinomial;
        var MultivariateBernoulli = dists.makeMultivariateBernoulli;
        var MultivariateGaussian = dists.makeMultivariateGaussian;
        var Poisson = dists.makePoisson;
        var RandomInteger = dists.makeRandomInteger;
        var SampleBasedMarginal = dists.makeSampleBasedMarginal;
        var TensorGaussian = dists.makeTensorGaussian;
        var TensorLaplace = dists.makeTensorLaplace;
        var Uniform = dists.makeUniform;
        var flip = function flip(globalStore, _k350, _address26, p) {
            var _currentAddress = _address26;
            _addr.save(_globalCurrentAddress, _address26);
            var _k353 = function (globalStore, _result352) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                var params = { p: _result352 };
                return function () {
                    return Bernoulli(globalStore, function (globalStore, _result351) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        return function () {
                            return sample(globalStore, _k350, _address26.concat('_51'), _result351);
                        };
                    }, _address26.concat('_50'), params);
                };
            };
            return function () {
                return ad.scalar.pneq(p, undefined) ? _k353(globalStore, p) : _k353(globalStore, 0.5);
            };
        };
        var mem = function mem(globalStore, _k306, _address41, f) {
            var _currentAddress = _address41;
            _addr.save(_globalCurrentAddress, _address41);
            var _k311 = function (globalStore, _result310) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                var _dummy309 = globalStore.memIndex = ad.scalar.add(1, _result310);
                var key = ad.scalar.add('mem', globalStore.memIndex);
                return function () {
                    return _k306(globalStore, function (globalStore, _k307, _address42) {
                        var _currentAddress = _address42;
                        _addr.save(_globalCurrentAddress, _address42);
                        var _arguments1 = Array.prototype.slice.call(arguments, 3);
                        var stringedArgs = ad.scalar.add(key, util.serialize(_arguments1));
                        return function () {
                            return _.has(globalStore, stringedArgs) ? _k307(globalStore, globalStore[stringedArgs]) : apply(globalStore, function (globalStore, val) {
                                _addr.save(_globalCurrentAddress, _currentAddress);
                                var _dummy308 = globalStore[stringedArgs] = val;
                                return function () {
                                    return _k307(globalStore, val);
                                };
                            }, _address42.concat('_69'), f, _arguments1);
                        };
                    });
                };
            };
            return function () {
                return globalStore.memIndex ? _k311(globalStore, globalStore.memIndex) : _k311(globalStore, 0);
            };
        };
        var error = function error(globalStore, _k174, _address121, msg) {
            var _currentAddress = _address121;
            _addr.save(_globalCurrentAddress, _address121);
            return function () {
                return _k174(globalStore, util.error(msg));
            };
        };
        var SampleGuide = function SampleGuide(globalStore, _k170, _address125, wpplFn, options) {
            var _currentAddress = _address125;
            _addr.save(_globalCurrentAddress, _address125);
            return function () {
                return ForwardSample(globalStore, _k170, _address125.concat('_154'), wpplFn, _.assign({ guide: !0 }, _.omit(options, 'guide')));
            };
        };
        var OptimizeThenSample = function OptimizeThenSample(globalStore, _k168, _address126, wpplFn, options) {
            var _currentAddress = _address126;
            _addr.save(_globalCurrentAddress, _address126);
            return function () {
                return Optimize(globalStore, function (globalStore, _dummy169) {
                    _addr.save(_globalCurrentAddress, _currentAddress);
                    var opts = _.pick(options, 'samples', 'onlyMAP', 'verbose');
                    return function () {
                        return SampleGuide(globalStore, _k168, _address126.concat('_156'), wpplFn, opts);
                    };
                }, _address126.concat('_155'), wpplFn, _.omit(options, 'samples', 'onlyMAP'));
            };
        };
        var DefaultInfer = function DefaultInfer(globalStore, _k158, _address127, wpplFn, options) {
            var _currentAddress = _address127;
            _addr.save(_globalCurrentAddress, _address127);
            var _dummy167 = util.mergeDefaults(options, {}, 'Infer');
            var maxEnumTreeSize = 200000;
            var minSampleRate = 250;
            var samples = 1000;
            return function () {
                return Enumerate(globalStore, function (globalStore, enumResult) {
                    _addr.save(_globalCurrentAddress, _currentAddress);
                    var _k166 = function (globalStore, _dummy165) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        var _dummy164 = console.log('Using "rejection"');
                        return function () {
                            return Rejection(globalStore, function (globalStore, rejResult) {
                                _addr.save(_globalCurrentAddress, _currentAddress);
                                return function () {
                                    return rejResult instanceof Error ? function (globalStore, _dummy163) {
                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                        return function () {
                                            return CheckSampleAfterFactor(globalStore, function (globalStore, hasSampleAfterFactor) {
                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                var _k161 = function (globalStore, _dummy160) {
                                                    _addr.save(_globalCurrentAddress, _currentAddress);
                                                    var _dummy159 = console.log('Using "MCMC"');
                                                    return function () {
                                                        return MCMC(globalStore, _k158, _address127.concat('_163'), wpplFn, { samples: samples });
                                                    };
                                                };
                                                return function () {
                                                    return hasSampleAfterFactor ? function (globalStore, _dummy162) {
                                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                                        return function () {
                                                            return SMC(globalStore, function (globalStore, smcResult) {
                                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                                return function () {
                                                                    return dists.isDist(smcResult) ? _k158(globalStore, smcResult) : smcResult instanceof Error ? _k161(globalStore, console.log(ad.scalar.add(smcResult.message, '..quit SMC'))) : error(globalStore, _k161, _address127.concat('_162'), 'Invalid return value from SMC');
                                                                };
                                                            }, _address127.concat('_161'), wpplFn, {
                                                                throwOnError: !1,
                                                                particles: samples
                                                            });
                                                        };
                                                    }(globalStore, console.log('Using "SMC" (interleaving samples and factors detected)')) : _k161(globalStore, undefined);
                                                };
                                            }, _address127.concat('_160'), wpplFn);
                                        };
                                    }(globalStore, console.log(ad.scalar.add(rejResult.message, '..quit rejection'))) : dists.isDist(rejResult) ? _k158(globalStore, rejResult) : error(globalStore, _k158, _address127.concat('_164'), 'Invalid return value from rejection');
                                };
                            }, _address127.concat('_159'), wpplFn, {
                                minSampleRate: minSampleRate,
                                throwOnError: !1,
                                samples: samples
                            });
                        };
                    };
                    return function () {
                        return dists.isDist(enumResult) ? _k158(globalStore, enumResult) : enumResult instanceof Error ? _k166(globalStore, console.log(ad.scalar.add(enumResult.message, '..quit enumerate'))) : error(globalStore, _k166, _address127.concat('_158'), 'Invalid return value from enumerate');
                    };
                }, _address127.concat('_157'), wpplFn, {
                    maxEnumTreeSize: maxEnumTreeSize,
                    maxRuntimeInMS: 5000,
                    throwOnError: !1,
                    strategy: 'depthFirst'
                });
            };
        };
        var Infer = function Infer(globalStore, _k151, _address128, options, maybeFn) {
            var _currentAddress = _address128;
            _addr.save(_globalCurrentAddress, _address128);
            var _k157 = function (globalStore, wpplFn) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                var _k156 = function (globalStore, _dummy155) {
                    _addr.save(_globalCurrentAddress, _currentAddress);
                    var methodMap = {
                        SMC: SMC,
                        MCMC: MCMC,
                        PMCMC: PMCMC,
                        asyncPF: AsyncPF,
                        rejection: Rejection,
                        enumerate: Enumerate,
                        incrementalMH: IncrementalMH,
                        forward: ForwardSample,
                        optimize: OptimizeThenSample,
                        defaultInfer: DefaultInfer
                    };
                    var _k154 = function (globalStore, methodName) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        var _k153 = function (globalStore, _dummy152) {
                            _addr.save(_globalCurrentAddress, _currentAddress);
                            var method = methodMap[methodName];
                            return function () {
                                return method(globalStore, _k151, _address128.concat('_167'), wpplFn, _.omit(options, 'method', 'model'));
                            };
                        };
                        return function () {
                            return _.has(methodMap, methodName) ? _k153(globalStore, undefined) : function (globalStore, methodNames) {
                                _addr.save(_globalCurrentAddress, _currentAddress);
                                var msg = ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add('Infer: \'', methodName), '\' is not a valid method. The following methods are available: '), methodNames.join(', ')), '.');
                                return function () {
                                    return error(globalStore, _k153, _address128.concat('_166'), msg);
                                };
                            }(globalStore, _.keys(methodMap));
                        };
                    };
                    return function () {
                        return options.method ? _k154(globalStore, options.method) : _k154(globalStore, 'defaultInfer');
                    };
                };
                return function () {
                    return _.isFunction(wpplFn) ? _k156(globalStore, undefined) : error(globalStore, _k156, _address128.concat('_165'), 'Infer: a model was not specified.');
                };
            };
            return function () {
                return util.isObject(options) ? maybeFn ? _k157(globalStore, maybeFn) : _k157(globalStore, options.model) : _k157(globalStore, options);
            };
        };
        var Vector = function Vector(globalStore, _k146, _address131, arr) {
            var _currentAddress = _address131;
            _addr.save(_globalCurrentAddress, _address131);
            var n = arr.length;
            var t = ad.tensor.fromScalars(arr);
            return function () {
                return _k146(globalStore, ad.tensor.reshape(t, [
                    n,
                    1
                ]));
            };
        };
        var csv = {
            read: webpplCsv.readCSV,
            write: webpplCsv.writeCSV,
            open: webpplCsv.openFile,
            close: webpplCsv.closeFile,
            writeLine: webpplCsv.writeLine,
            writeMarginals: webpplCsv.writeMarginals,
            writeJoint: webpplCsv.writeJoint,
            writeDistTable: webpplCsv.writeDistTable
        };
        var args = process.argv.slice(2);
        return function () {
            return mem(globalStore, function (globalStore, T) {
                _addr.save(_globalCurrentAddress, _currentAddress);
                return function () {
                    return mem(globalStore, function (globalStore, trustworthiness) {
                        _addr.save(_globalCurrentAddress, _currentAddress);
                        return function () {
                            return mem(globalStore, function (globalStore, strategy) {
                                _addr.save(_globalCurrentAddress, _currentAddress);
                                return function () {
                                    return mem(globalStore, function (globalStore, extractAnnotatorStringIntoArray) {
                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                        return function () {
                                            return mem(globalStore, function (globalStore, extractAnswerStringIntoArray) {
                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                var model = function model(globalStore, _k3, _address174) {
                                                    var _currentAddress = _address174;
                                                    _addr.save(_globalCurrentAddress, _address174);
                                                    var threadNumber = args[3];
                                                    var questionData = csv.read(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add('../MACE/100DataSetPreparation/020ProcessedData/Formats/030QuestionsPlacedTogether/segmentData/', '0'), threadNumber), '0'), 'AGWSegment.csv'));
                                                    var _dummy16 = questionData.pop();
                                                    return function () {
                                                        return mapData(globalStore, function (globalStore, modelOverQuestions) {
                                                            _addr.save(_globalCurrentAddress, _currentAddress);
                                                            return function () {
                                                                return _k3(globalStore, modelOverQuestions);
                                                            };
                                                        }, _address174.concat('_275'), { data: questionData }, function (globalStore, _k4, _address175, question, index) {
                                                            var _currentAddress = _address175;
                                                            _addr.save(_globalCurrentAddress, _address175);
                                                            return function () {
                                                                return T(globalStore, function (globalStore, _result15) {
                                                                    _addr.save(_globalCurrentAddress, _currentAddress);
                                                                    var trueLabel = _result15.indexOf(1);
                                                                    return function () {
                                                                        return trustworthiness(globalStore, function (globalStore, _result14) {
                                                                            _addr.save(_globalCurrentAddress, _currentAddress);
                                                                            return function () {
                                                                                return flip(globalStore, function (globalStore, S) {
                                                                                    _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                    return function () {
                                                                                        return S ? strategy(globalStore, function (globalStore, _result10) {
                                                                                            _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                            return function () {
                                                                                                return Multinomial(globalStore, function (globalStore, _result9) {
                                                                                                    _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                    return function () {
                                                                                                        return sample(globalStore, function (globalStore, _result8) {
                                                                                                            _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                            var A = _result8.indexOf(1);
                                                                                                            var _k7 = function (globalStore, individualScore) {
                                                                                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                                return function () {
                                                                                                                    return factor(globalStore, function (globalStore, _dummy6) {
                                                                                                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                                        return function () {
                                                                                                                            return trustworthiness(globalStore, function (globalStore, _result5) {
                                                                                                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                                                return function () {
                                                                                                                                    return _k4(globalStore, [
                                                                                                                                        question[0],
                                                                                                                                        trueLabel,
                                                                                                                                        A,
                                                                                                                                        question[2],
                                                                                                                                        _result5
                                                                                                                                    ]);
                                                                                                                                };
                                                                                                                            }, _address175.concat('_272'), question[2]);
                                                                                                                        };
                                                                                                                    }, _address175.concat('_271'), individualScore);
                                                                                                                };
                                                                                                            };
                                                                                                            return function () {
                                                                                                                return ad.scalar.peq(A.toString(), question[1]) ? _k7(globalStore, 1) : _k7(globalStore, 0);
                                                                                                            };
                                                                                                        }, _address175.concat('_270'), _result9);
                                                                                                    };
                                                                                                }, _address175.concat('_269'), {
                                                                                                    ps: Array.from(_result10.data),
                                                                                                    n: 1
                                                                                                });
                                                                                            };
                                                                                        }, _address175.concat('_268'), question[2]) : function (globalStore, A) {
                                                                                            _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                            var _k13 = function (globalStore, individualScore) {
                                                                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                return function () {
                                                                                                    return factor(globalStore, function (globalStore, _dummy12) {
                                                                                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                        return function () {
                                                                                                            return trustworthiness(globalStore, function (globalStore, _result11) {
                                                                                                                _addr.save(_globalCurrentAddress, _currentAddress);
                                                                                                                return function () {
                                                                                                                    return _k4(globalStore, [
                                                                                                                        question[0],
                                                                                                                        trueLabel,
                                                                                                                        A,
                                                                                                                        question[2],
                                                                                                                        _result11
                                                                                                                    ]);
                                                                                                                };
                                                                                                            }, _address175.concat('_274'), question[2]);
                                                                                                        };
                                                                                                    }, _address175.concat('_273'), individualScore);
                                                                                                };
                                                                                            };
                                                                                            return function () {
                                                                                                return ad.scalar.peq(A.toString(), question[1]) ? _k13(globalStore, 1) : _k13(globalStore, 0);
                                                                                            };
                                                                                        }(globalStore, trueLabel);
                                                                                    };
                                                                                }, _address175.concat('_267'), ad.scalar.sub(1, _result14));
                                                                            };
                                                                        }, _address175.concat('_266'), question[2]);
                                                                    };
                                                                }, _address175.concat('_265'), question[0]);
                                                            };
                                                        });
                                                    };
                                                };
                                                var _dummy2 = console.time('InferenceTimer');
                                                var sampleSize = Number.parseFloat(args[0]);
                                                var burnSize = Number.parseFloat(args[1]);
                                                var lagSize = Number.parseFloat(args[2]);
                                                var threadNumber = args[3];
                                                var trialNumber = args[4];
                                                var modelType = args[5];
                                                var mainTrialName = ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add('0', trialNumber), '0'), 'S'), sampleSize.toString()), 'B'), burnSize.toString()), 'L'), lagSize.toString()), modelType);
                                                var threadDataName = ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add('0', threadNumber), '0'), 'Result'), 'S'), sampleSize.toString()), 'B'), burnSize.toString()), 'L'), lagSize.toString());
                                                return function () {
                                                    return Infer(globalStore, function (globalStore, finalResultTest) {
                                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                                        var _dummy1 = csv.writeMarginals(finalResultTest, ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add(ad.scalar.add('../MACE/200Model/030IntermediateDataRepository/', mainTrialName), '/0'), threadNumber), '0ThreadData/'), threadDataName), '.csv'));
                                                        return function () {
                                                            return _k0(globalStore, console.timeEnd('InferenceTimer'));
                                                        };
                                                    }, _address0.concat('_276'), {
                                                        method: 'MCMC',
                                                        samples: sampleSize,
                                                        burn: burnSize,
                                                        lag: lagSize,
                                                        verbose: !0,
                                                        onlyMAP: !1
                                                    }, model);
                                                };
                                            }, _address0.concat('_260'), function (globalStore, _k23, _address167, data) {
                                                var _currentAddress = _address167;
                                                _addr.save(_globalCurrentAddress, _address167);
                                                return function () {
                                                    return _k23(globalStore, data.match(/\d+/g));
                                                };
                                            });
                                        };
                                    }, _address0.concat('_259'), function (globalStore, _k24, _address166, data) {
                                        var _currentAddress = _address166;
                                        _addr.save(_globalCurrentAddress, _address166);
                                        return function () {
                                            return _k24(globalStore, data.replace('[', '').replace(']', '').split(','));
                                        };
                                    });
                                };
                            }, _address0.concat('_258'), function (globalStore, _k25, _address165, annotator) {
                                var _currentAddress = _address165;
                                _addr.save(_globalCurrentAddress, _address165);
                                return function () {
                                    return Vector(globalStore, function (globalStore, _result26) {
                                        _addr.save(_globalCurrentAddress, _currentAddress);
                                        return function () {
                                            return dirichlet(globalStore, _k25, _address165.concat('_257'), { alpha: _result26 });
                                        };
                                    }, _address165.concat('_256'), [
                                        10,
                                        10,
                                        10,
                                        10,
                                        10,
                                        10,
                                        10,
                                        10
                                    ]);
                                };
                            });
                        };
                    }, _address0.concat('_255'), function (globalStore, _k27, _address164, annotator) {
                        var _currentAddress = _address164;
                        _addr.save(_globalCurrentAddress, _address164);
                        return function () {
                            return beta(globalStore, _k27, _address164.concat('_254'), {
                                a: 0.5,
                                b: 0.5
                            });
                        };
                    });
                };
            }, _address0.concat('_253'), function (globalStore, _k28, _address163, question) {
                var _currentAddress = _address163;
                _addr.save(_globalCurrentAddress, _address163);
                return function () {
                    return categorical(globalStore, _k28, _address163.concat('_252'), {
                        vs: [
                            [
                                1,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0
                            ],
                            [
                                0,
                                1,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0
                            ],
                            [
                                0,
                                0,
                                1,
                                0,
                                0,
                                0,
                                0,
                                0
                            ],
                            [
                                0,
                                0,
                                0,
                                1,
                                0,
                                0,
                                0,
                                0
                            ],
                            [
                                0,
                                0,
                                0,
                                0,
                                1,
                                0,
                                0,
                                0
                            ],
                            [
                                0,
                                0,
                                0,
                                0,
                                0,
                                1,
                                0,
                                0
                            ],
                            [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                1,
                                0
                            ],
                            [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                1
                            ]
                        ],
                        ps: [
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8),
                            ad.scalar.div(1, 8)
                        ]
                    });
                };
            });
        };
    });
});

webppl.runEvaled(main, __runner__, {}, {}, topK, '');