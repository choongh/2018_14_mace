
# This object is a standardized answer key for replacing an answer to a quiz with a numerical value.
hardCodedAnswerKey = {
	"Good" : "0",
	"Bad" : "1",
	"Neutral (neither good nor bad)" : "2",
	"Can't tell because in this context 'verb' has more than one meaning" : "3",
    "Depends too much on context. Flag for further investigation." : "4",
	"Can't tell because the sentence is ungrammatical." : "5",
    "other" : "6",
    "Can't tell because I don't know that verb." : "7"
}

