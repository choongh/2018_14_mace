# Author: Han Xiang Choong
# Date: 08/03/2018
# File: 040CutterQSI.py
# Status: Ongoing
# Description: This is used to segment QuestionSets and Indiscriminate data. To do this, it relies
#              on numerical total and divider values passed to it by 010DataPrepController.sh, which
#              determines the size of each segment . 

import csv
import sys

inputPath = sys.argv[1]
outputPath = sys.argv[2]
divider = int(sys.argv[3])
total = int(sys.argv[4])

def segmentDataSetsAndIndiscriminate(inputPath, outputPath, divider, total):

    #Append all of the questions in the dataset to an array for slicing. 
    questions = []
    with open(inputPath, 'rU') as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            questions.append(row)
    
    # Segment will be ued to hold all of the annotations for a given segment.
    segment = []
    # rowCount keeps track of individual annotations.
    # It is used for signaling the appropriate time to cut a section of data into a segment.
    rowCount = 1
    # segmentCount keeps track of the number of individual groups. 
    # It is used for indexing and naming. 
    segmentCount = 1

    for i in range(0, total):
        if (rowCount % divider == 0): # If the rowCount divided by the divider, say 19, is 0, then there are 
            # 19 rows in the segment array, necessitating writing out and then clearing the contents. 
            # Append the current question, then initialize the file for that segment. 
            segment.append(questions[i])
            segmentOutputPath = outputPath + "0" + str(segmentCount) + "0AGWSegment.csv"
            # Write the contents of segment array. 
            with open(segmentOutputPath, 'w', newline='') as outputFile:
                writer = csv.writer(outputFile)
                for question in segment:
                    writer.writerow(question)
            # Increment counters, reset segment array for the next section. 
            segmentCount += 1
            rowCount += 1
            segment = []
        else:
            # If the above modulo condition doesn't hold, then we're in the process of filling the
            # segment array. 
            rowCount += 1
            segment.append(questions[i])

        # This section deals with the remaining entries, which may be in a segment array, but not number
        # enough entries to have been written out above. We output these last entries as a slightly 
        # smaller segment of their own. 
        if int(rowCount) == int(total):
            if len(segment) > 0:
                segmentOutputPath = outputPath + "0" + str(segmentCount) + "0AGWSegment.csv"
                with open(segmentOutputPath, 'w', newline='') as outputFile:
                    writer = csv.writer(outputFile)
                    for question in segment:
                        writer.writerow(question)

# Function call
segmentDataSetsAndIndiscriminate(inputPath, outputPath, divider, total)

