# Author: Han Xiang Choong
# Date: 07/24/2018
# File: 060CounterProcessor.py
# Status: Ongoing
# Description: Prepare to run MACE by counting up the responses given to a question in the original dataset. 

import csv
import sys
import AnswerKeyObject as AKO

inputPath = sys.argv[1]
outputPath = sys.argv[2]
nChoices = int(sys.argv[3])

# This helper function serves to create a dictionary which will hold a vector of answer distributions.
# nChoices represents the number of possible answers for a question.
def initializeCounterDictionary(nChoices):
    output = {}
    for i in range(0, nChoices):
        output.update({str(i) : 0})
    return output

# This helper function initializes a dictionary where questions are mapped as keys to dictionaries created
# by initializeCounterDictionary above. Essentially, each question is associated with a vector of form
# [0,0,1,23,30,40,5,2], represented in dictionary form.
def initializeQuestionDictionary(inputPath, nChoices):
    output = {}
    with open(inputPath, "rU") as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if(row[1] != "question_id"):
                # Take only the oversampled entries
                if(row[5] == "true"):
                    output.update({row[2] : initializeCounterDictionary(nChoices)})
    return output

# This function takes the base data file, and creates a counts file. It does this by summing the number of responses to a question,
# creating a distribution of the form [30, 1, 0, 23, 14], and associates it with that question in a csv file. 
def prepareCounts(inputPath, outputPath, nChoices):
    # Open input data - This will be formattedAGWData or synthData.
    output = initializeQuestionDictionary(inputPath, nChoices)
    with open(inputPath, "rU") as inputFile:
        # Open output counts file.
        with open(outputPath, "w", newline='') as outputFile:
            reader = csv.reader(inputFile)
            writer = csv.writer(outputFile)
            # Write header first. 
            writer.writerow(["Question","Good","Bad","Neutral","Don't Know Meaning", "Depends","Ungrammatical", "Other", "Do not know verb"])
            # For each component forming a question. 
            for row in reader:
                if row[2] in output:
                    answer = row[3].replace('\\', '')
                    if "," in answer:
                        # Loop over all answers given for a question
                        for component in answer.split(", "):
                            answer = component
                            if "because in this context" in answer:
                                answer = "3"
                                currentCount = output[row[2]][answer] + 1
                                output[row[2]].update({answer : currentCount}) 
                            # If the answer is not too troublesome
                            else:
                                answer = AKO.hardCodedAnswerKey[answer]
                                currentCount = output[row[2]][answer] + 1
                                output[row[2]].update({answer : currentCount})
                # Dealing with this difficult answer outside the context of multiple valid answers.
                    elif "because in this context" in answer:
                        answer = "3"
                        currentCount = output[row[2]][answer] + 1
                        output[row[2]].update({answer : currentCount})
                    # Regular case (No multiple answers or difficult choices)
                    else:
                        answer = AKO.hardCodedAnswerKey[answer]
                        currentCount = output[row[2]][answer] + 1
                        output[row[2]].update({answer : currentCount})
            for key in output:
                writer.writerow([key, output[key]["0"], output[key]["1"], output[key]["2"], output[key]["3"], output[key]["4"], output[key]["5"], output[key]["6"], output[key]["7"]])

prepareCounts(inputPath, outputPath, nChoices)