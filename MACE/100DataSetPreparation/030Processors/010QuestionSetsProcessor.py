# Author: Han Xiang Choong
# Date: 07/23/2018
# File: 010QuestionSetsProcessor.py
# Status: Ongoing
# Description: Process data for MACE by reformatting response data set into an annotation matrix.
#              This matrix is a question associated with an array of answers, and an array of annotators.
#              These arrays match by index. 

import csv
import sys
import AnswerKeyObject as AKO

# This dictionary is used for converting answers between sentence and number format. 
mainData = sys.argv[1]
outputPath = sys.argv[2]

def parseAGWQuestionSets(mainData, outputPath):

    questionDict = {}
    annotatorDict = {}
    # Populate a dictionary for directed updating later. Insert all questions. 
    with open(mainData, 'rU') as inputFile:
        reader = csv.reader(inputFile)
        for question in reader:
            # Take only the oversampled entries
            if(question[5] == "true"):
                # To these two dictionaries, add the actual question as the key to an empty array. 
                # The idea is to update both of these arrays simultaneously, such that questionDict
                # keeps track of answers given, and annotatorDict keeps track of who gave those 
                # responses, with the help of consistent indexing. 
                questionDict.update({question[2] : []})
                annotatorDict.update({question[2] : []})
    inputFile.close()
    
    # Attach annotators and questions to the previous dictionaries. 
    with open(mainData, 'rU') as inputFile:
        reader = csv.reader(inputFile)
        # For each row in mainData file
        for row in reader:
            # Ignore the header row
            if(row[1] != "question_id"):
                # Take only the oversampled entries
                if(row[5] == "true"):
                    # Eliminate all backward slashes
                    answer = row[3].replace('\\', '')
                    # Deal with multiple answers. One question may have more than one answer from a user.
                    # This is treated as a new entry regardless. 
                    if "," in answer:
                        # Loop over all answers given for a question
                        for component in answer.split(", "):
                            # This answer below relies on the actual verb, so does not easily call dictionary for reference
                            if "because in this context" in answer:
                                questionDict[row[2]].append("3")
                            # If the answer is not too troublesome
                            else:
                                questionDict[row[2]].append(AKO.hardCodedAnswerKey[component])
                            annotatorDict[row[2]].append(row[4])
                    # Dealing with this difficult answer outside the context of multiple valid answers.
                    elif "because in this context" in answer:
                        questionDict[row[2]].append("3")
                        annotatorDict[row[2]].append(row[4])
                    # Regular case (No multiple answers or difficult choices)
                    else:
                        questionDict[row[2]].append(AKO.hardCodedAnswerKey[answer])
                        annotatorDict[row[2]].append(row[4])
    inputFile.close()

    # Now write all of the data associated with that question.
    with open(outputPath, 'w', newline='') as outputFile:
        writer = csv.writer(outputFile)
        for question in questionDict:
            writer.writerow([question, questionDict[question], annotatorDict[question]])

# Call Function

parseAGWQuestionSets(mainData, outputPath)