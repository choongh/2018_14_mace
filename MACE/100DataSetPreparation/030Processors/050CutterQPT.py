# Author: Han Xiang Choong
# Date: 08/03/2018
# File: 050CutterQPT.py
# Status: Ongoing
# Description: This function is for cutting QPT format data. It works differently from the last 
#              cutter, since it relies on using dictionaries for sorting questions into distinct groups. 

import csv
import sys

inputPath = sys.argv[1]
outputPath = sys.argv[2]

def segmentData(inputPath, outputPath):

    segmentDict = {}

    # First initialize the output dictionary with all questions attached to empty arrays. 
    with open(inputPath, 'rU') as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            segmentDict.update({row[0] : []})
    inputFile.close()

    # For each annotation, use intermediateArray to hold that annotation, and then append it
    # to the appropriate entry in the dictionary. In this way, we take each unique question
    # and map it to an array of annotations for that question. 
    with open(inputPath, 'rU') as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            intermediateArray = segmentDict[row[0]]
            intermediateArray.append(row)
            segmentDict.update({row[0] : intermediateArray})
    inputFile.close()
            
    segmentCount = 1
    
    # Once all annoations have been assigned to the appropriate entry in the dictionary, 
    # we can write each entry to its own separate segment. 
    for question in segmentDict:
        segmentOutputPath = outputPath + "0" + str(segmentCount) + "0AGWSegment.csv"
        with open(segmentOutputPath, 'w', newline='') as outputFile:
            writer = csv.writer(outputFile)
            for annotation in segmentDict[question]:
                writer.writerow(annotation)
        segmentCount += 1
            
segmentData(inputPath, outputPath)