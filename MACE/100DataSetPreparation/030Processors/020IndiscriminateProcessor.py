# Author: Han Xiang Choong
# Date: 07/23/2018
# File: 020IndiscriminateProcessor.py
# Status: Ongoing
# Description: Process data for MACE by converting all answers given to numerical values, using
#              the AnswerKeyObject. No further organization is necessary for this format. 

import csv
import sys
import AnswerKeyObject as AKO
 
# This dictionary is used for converting answers between sentence and number format. 
mainData = sys.argv[1]
outputPath = sys.argv[2]

def parseAGWIndiscriminate(mainData, outputPath):
    with open(mainData, 'rU') as inputFile:
        with open(outputPath, 'w', newline='') as outputFile:
            writer = csv.writer(outputFile)
            reader = csv.reader(inputFile)
            # For each row in mainData file
            for row in reader:
                if(row[1] != "question_id"): # Ignore the header row
                    if(row[5] == "true"): # Take only the oversampled entries
                        # Eliminate all backward slashes
                        answer = row[3].replace('\\', '')
                        # Deal with multiple answers. One question may have more than one answer from a user.
                        # This is treated as a new entry regardless. 
                        if "," in answer:
                            # Loop over all answers given for a question
                            for component in answer.split(", "):
                                # This answer below relies on the actual verb, so does not easily call dictionary for reference
                                if "because in this context" in answer:
                                    writer.writerow([row[2], "3", row[4]])
                                # If the answer is not too troublesome
                                else:
                                    writer.writerow([row[2], AKO.hardCodedAnswerKey[component], row[4]])
                        # Dealing with this difficult answer outside the context of multiple valid answers.
                        elif "because in this context" in answer:
                            writer.writerow([row[2], "3", row[4]])
                        # Regular case (No multiple answers or difficult choices)
                        else:
                            writer.writerow([row[2], AKO.hardCodedAnswerKey[answer], row[4]])
    inputFile.close()

parseAGWIndiscriminate(mainData, outputPath)