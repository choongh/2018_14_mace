# Author: Han Xiang Choong
# Date: 07/23/2018
# File: 030QPTProcessor.py
# Status: Ongoing
# Description: Process data for MACE by reformatting response data set. Replace answers
#              with numerical values, and place like questions together in blocks of their
#              own. 

import csv
import sys
import AnswerKeyObject as AKO

# This dictionary is used for converting answers between sentence and number format. 
mainData = sys.argv[1]
outputPath = sys.argv[2]

def parseAGWOrganized(mainData, outputPath):

    annotationDict = {}
    # Create this annotationDict for the purpose of keeping track of answers given to each question.
    # This will be helpful for writing out later. 
    with open(mainData, 'rU') as inputFile:
        reader = csv.reader(inputFile)
        for row in reader:
            if(row[5] == "true"): # Take only the oversampled entries
                annotationDict.update({row[2] : []})
    inputFile.close()

    # Attach annotators and questions to the previous dictionaries. 
    with open(mainData, 'rU') as inputFile:
        with open(outputPath, 'w', newline='') as outputFile:
            writer = csv.writer(outputFile)
            reader = csv.reader(inputFile)
            # For each row in mainData file
            for row in reader:
                if(row[1] != "question_id"): # Ignore the header row
                    if(row[5] == "true"): # Take only the oversampled entries
                        # Eliminate all backward slashes
                        answer = row[3].replace('\\', '')
                        # Deal with multiple answers. One question may have more than one answer from a user.
                        # For this multiple answer condition, we treat each answer as a separate annotation. 
                        # This is treated as a new entry regardless. 
                        if "," in answer:
                            # Loop over all answers given for a question
                            for component in answer.split(", "):
                                # This answer below relies on the actual verb, so does not easily call dictionary for reference
                                if "because in this context" in answer:
                                    # If there's a match, then grab the array associated with the question.
                                    intermediate = annotationDict[row[2]]
                                    # To that array, append in this order: The question, the answer, the annotator's ID. 
                                    intermediate.append([row[2], "3", row[4]])
                                    # Now update the dictionary with the newly updated array for that question. 
                                    annotationDict.update({row[2] : intermediate})
                                # If the answer is not too troublesome
                                else:
                                    intermediate = annotationDict[row[2]]
                                    intermediate.append([row[2], AKO.hardCodedAnswerKey[component], row[4]])
                                    annotationDict.update({row[2] : intermediate})

                        # Dealing with this difficult answer outside the context of multiple valid answers.
                        elif "because in this context" in answer:
                            intermediate = annotationDict[row[2]]
                            intermediate.append([row[2], "3", row[4]])
                            annotationDict.update({row[2] : intermediate})
 
                        # Regular case (No multiple answers or difficult choices)
                        else:
                            intermediate = annotationDict[row[2]]
                            intermediate.append([row[2], AKO.hardCodedAnswerKey[answer], row[4]])
                            annotationDict.update({row[2] : intermediate})
    inputFile.close()

    # At the end of the above process, there is now a dictionary of questions mapped to answers and annotators.
    # Writing this out in order creates the desired effect of blocks of distinct questions. 
    with open(outputPath, 'w', newline='') as outputFile:
        writer = csv.writer(outputFile)
        for question in annotationDict:
            for annotation in annotationDict[question]:
                writer.writerow(annotation)
parseAGWOrganized(mainData, outputPath)